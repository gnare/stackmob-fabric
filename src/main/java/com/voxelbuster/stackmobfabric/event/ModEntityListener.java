package com.voxelbuster.stackmobfabric.event;

import com.mojang.logging.LogUtils;
import com.voxelbuster.stackmobfabric.config.EnumModifyHandlingAction;
import com.voxelbuster.stackmobfabric.data.IStackedEntityComponent;
import com.voxelbuster.stackmobfabric.util.MathUtil;
import lombok.NonNull;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerWorldEvents;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.animal.Bee;
import net.minecraft.world.entity.animal.Cat;
import net.minecraft.world.entity.animal.Chicken;
import net.minecraft.world.entity.animal.Sheep;
import net.minecraft.world.entity.animal.Turtle;
import net.minecraft.world.entity.animal.Wolf;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Slime;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.entity.EntityTypeTest;
import net.minecraft.world.phys.AABB;
import org.slf4j.Logger;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import static com.voxelbuster.stackmobfabric.StackMobFabric.config;
import static com.voxelbuster.stackmobfabric.config.EnumDeathHandlingAction.*;
import static com.voxelbuster.stackmobfabric.data.StackMobComponents.STACKED_ENTITIES;
import static com.voxelbuster.stackmobfabric.data.StackMobComponents.STACKED_NAMEABLE;

/**
 * Contains many event handlers for the mod. Most of the event handlers are called by mixins.
 * The event handlers are responsible for handling the logic of the mod, such as merging entities,
 * handling entity deaths, and handling entity taming.
 */
public class ModEntityListener implements ServerTickEvents.EndWorldTick, ServerWorldEvents.Load {

    @SuppressWarnings("unused")
    private static final Logger log = LogUtils.getLogger();

    private Scheduler scheduler;

    /**
     * Registers the event listeners
     */
    public void register() {
        ServerTickEvents.END_WORLD_TICK.register(this);
        ServerWorldEvents.LOAD.register(this);
    }

    /**
     * Gets all the entities that are applicable for stacking. Only applies to entities that are in loaded
     * and simulating chunks. Abides by parameters set by the user in the config.
     *
     * @param world The world to get the entities from
     * @return A list of entities that are applicable for stacking
     */
    @SuppressWarnings("squid:S1319")
    public ArrayList<Entity> getApplicableEntities(ServerLevel world) {
        ArrayList<Entity> applicableEntities;

        if (config.stackMobs) {
            applicableEntities = new ArrayList<>(
                world.getEntities(EntityTypeTest.forClass(LivingEntity.class),
                    e -> e.isAlive() &&
                        e.getType() != EntityType.PLAYER &&
                        config.stackVillagers || !(e instanceof Villager)
                )
            );
        } else {
            applicableEntities = new ArrayList<>();
        }

        return applicableEntities;
    }

    /**
     * Called when a world is loaded. Initializes the task scheduler.
     *
     * @param server The server currently connected to
     * @param world  The world that is currently loaded
     */
    @Override
    public void onWorldLoad(MinecraftServer server, ServerLevel world) {
        scheduler = new Scheduler(server);
    }

    /**
     * Called at the end of each tick. Merges entities together if they are applicable. Ticks the scheduler.
     *
     * @param world The currently loaded world
     */
    @Override
    public void onEndTick(ServerLevel world) {
        scheduler.tick();

        ArrayList<Entity> applicableEntities = getApplicableEntities(world);

        if (applicableEntities.isEmpty()) {
            return;
        }

        // Selects a number of entities randomly based on the config's processing rate
        ThreadLocalRandom.current().ints(config.processingRate, 0, applicableEntities.size())
            .forEach(i -> {
                Entity entity = applicableEntities.get(i);
                mergeEntities(world, entity, applicableEntities);
            });
    }

    /**
     * Merges entities together if they are applicable. This is the main event logic for the mod.
     * This checks various entity properties to determine if they are eligible for stacking, as determined
     * by the config.
     *
     * @param world              The currently loaded world
     * @param entity             The entity to check for stacking
     * @param applicableEntities A list of entities that are applicable for stacking
     */
    @SuppressWarnings("squid:S5803")
    private static void mergeEntities(ServerLevel world, Entity entity, ArrayList<Entity> applicableEntities) {
        if (entity.tickCount < config.processDelay && !(entity instanceof ItemEntity)) {
            return;
        }

        if (entity instanceof TamableAnimal tamableAnimalNearby && tamableAnimalNearby.isTame()) {
            return;
        }

        if (!config.stackBees && entity instanceof Bee) {
            return;
        }

        if (entity instanceof Bee bee && bee.hasNectar()) {
            return;
        }

        if (entity.isRemoved()) {
            return;
        }

        if (config.getExcludedEntityTypes().contains(entity.getType())) {
            return;
        }

        IStackedEntityComponent parentComponent = entity.getComponent(STACKED_ENTITIES);

        if (
            !entity.getComponent(STACKED_NAMEABLE).getCustomName().isEmpty()
                && parentComponent.getStackedEntities().isEmpty()
                && !(entity instanceof ItemEntity)
        ) {
            return;
        }

        int searchRadius = config.stackSearchRadius;

        if (entity instanceof Slime slime) {
            searchRadius = Math.max(1, slime.getSize() / 5) * config.stackSearchRadius;
        }

        // Finds nearby entities that are of the same type
        List<Entity> nearby = world.getEntities(
            entity,
            // create AABB centered on entity with radius 10
            new AABB(entity.getX() - searchRadius,
                entity.getY() - searchRadius,
                entity.getZ() - searchRadius,

                entity.getX() + searchRadius,
                entity.getY() + searchRadius,
                entity.getZ() + searchRadius),
            e -> e.getClass().isInstance(entity) && applicableEntities.contains(e)
        );

        if (nearby.size() < 2 && parentComponent.getStackedEntities().isEmpty() && !(entity instanceof ItemEntity)) {
            return;
        }


        // Processes nearby entities and merges them if eligible
        nearby.forEach(nearbyEntity -> {
            if (nearbyEntity.tickCount < config.processDelay && !(nearbyEntity instanceof ItemEntity)) {
                return;
            }

            // Serialize entity
            IStackedEntityComponent nearbyComponent = nearbyEntity.getComponent(STACKED_ENTITIES);

            if (
                !nearbyEntity.getComponent(STACKED_NAMEABLE).getCustomName().isEmpty() &&
                    nearbyComponent.getStackedEntities().isEmpty() &&
                    !(nearbyEntity instanceof ItemEntity)
            ) {
                return;
            }

            // Check entity eligibility
            if (nearbyEntity instanceof LivingEntity nearbyLiving && entity instanceof LivingEntity parentLiving) {
                if (nearbyLiving.isBaby() != parentLiving.isBaby()) {
                    return;
                } else if (!config.stackBabies && nearbyLiving.isBaby() && parentLiving.isBaby()) {
                    return;
                } else if (!config.stackNonBabies && !nearbyLiving.isBaby() && !parentLiving.isBaby()) {
                    return;
                } else if (config.requireLineOfSight &&
                    (!nearbyLiving.hasLineOfSight(parentLiving) || !parentLiving.hasLineOfSight(nearbyLiving))
                ) {
                    return;
                } else if (nearbyLiving.isDeadOrDying() || parentLiving.isDeadOrDying()) {
                    return;
                } else if (!config.stackTamed &&
                    (nearbyEntity instanceof TamableAnimal tamableAnimalNearby && tamableAnimalNearby.isTame())
                ) {
                    return;
                } else if (
                    nearbyEntity instanceof TamableAnimal tamableAnimalNearby &&
                        entity instanceof TamableAnimal tamableAnimal &&
                        tamableAnimalNearby.isTame() != tamableAnimal.isTame()
                ) {
                    return;
                } else if (!parentLiving.getPassengers().isEmpty() || !nearbyLiving.getPassengers().isEmpty()) {
                    return;
                } else if (nearbyEntity instanceof Slime nearSlime && entity instanceof Slime parentSlime) {
                    if (nearSlime.getSize() != parentSlime.getSize()) {
                        return;
                    }
                } else if (
                    nearbyEntity instanceof Sheep nearbySheep &&
                        entity instanceof Sheep parentSheep &&
                        nearbySheep.isSheared() != parentSheep.isSheared()
                ) {
                    return;
                } else if (!config.stackBees && nearbyEntity instanceof Bee) {
                    return;
                } else if (nearbyEntity instanceof Bee nearbyBee && nearbyBee.hasNectar()) {
                    return;
                }
            }

            if (!entity.getClass().isInstance(nearbyEntity)) {
                throw new IllegalStateException(
                    "Entities are not related classes: %s != %s"
                        .formatted(
                            entity.getClass().getName(),
                            nearbyEntity.getClass().getName()
                        )
                );
            }

            // Merges same size slimes and increases their size
            if (entity instanceof Slime slime && nearbyEntity instanceof Slime nearbySlime && config.increaseSlimeSize) {
                if (MathUtil.near(slime.getSize(), nearbySlime.getSize(), 1)) {
                    if (slime.getSize() < 40) {
                        if (slime.getSize() < nearbySlime.getSize()) {
                            slime.setSize(Math.min(40, nearbySlime.getSize() + 1), true);
                        } else {
                            slime.setSize(Math.min(40, slime.getSize() + 1), true);
                        }
                        nearbySlime.remove(Entity.RemovalReason.DISCARDED);
                        return;
                    }
                } else {
                    return;
                }
            }

            // Merge already stored entities
            parentComponent.getStackedEntities().addAll(nearbyComponent.getStackedEntities());
            nearbyComponent.getStackedEntities().clear();

            CompoundTag newTag = new CompoundTag();
            nearbyEntity.save(newTag);
            parentComponent.getStackedEntities().add(newTag);

            nearbyEntity.getComponent(STACKED_NAMEABLE).setStackSize(1);

            // Delete the entity
            nearbyEntity.remove(Entity.RemovalReason.DISCARDED);
        });

        if (entity instanceof ItemEntity) {
            return;
        }

        int stackSize = parentComponent.getStackedEntities().size() + 1;

        if (stackSize < 2) {
            return;
        }

        // Set the entity name and displayed stack size
        entity.getComponent(STACKED_NAMEABLE).setStackSize(stackSize);
    }

    /**
     * Called when an entity is damaged. Sets the last hurt value for the entity.
     * Called by
     * {@link com.voxelbuster.stackmobfabric.mixin.MixinLivingEntity#injectHurt(DamageSource, float, CallbackInfoReturnable) MixinLivingEntity}.
     *
     * @param entity The entity that was damaged
     * @param source The source of the damage
     * @param amount The amount of damage dealt
     */
    @SuppressWarnings("unused")
    public void onEntityDamaged(LivingEntity entity, DamageSource source, float amount) {
        entity.getComponent(STACKED_ENTITIES).setLastHurtValue(amount);
        entity.getComponent(STACKED_ENTITIES).setLastHpValue(entity.getHealth());
    }

    /**
     * Called when an entity dies. Handles the death of the entity based on the config.
     * Called by
     * {@link com.voxelbuster.stackmobfabric.mixin.MixinLivingEntity#injectDie(DamageSource, CallbackInfo) MixinLivingEntity}.
     *
     * @param died   The entity that died
     * @param source The source of the damage
     */
    public void onEntityDeath(LivingEntity died, DamageSource source) {
        if (source.equals(died.damageSources().genericKill()) || died.getComponent(STACKED_ENTITIES).shouldSkipDeathEvents()) {
            return;
        }

        int countEntity = died.getComponent(STACKED_ENTITIES).getStackedEntities().size();
        if (countEntity < 1) {
            return;
        }

        if (config.deathAction == ALL) {
            for (int i = 0; i < countEntity - 1; i++) {
                died.dropAllDeathLoot(((ServerLevel) died.level()), source);
            }

        } else if (config.deathAction == RANDOM) {
            int randomIndex = ThreadLocalRandom.current().nextInt(countEntity - 1);
            LivingEntity slicedEntity = (LivingEntity) died.getComponent(STACKED_ENTITIES).sliceOne(died.level());
            slicedEntity.getComponent(STACKED_ENTITIES).applyConsumerToAll(
                e -> e.getComponent(STACKED_ENTITIES).setSkipDeathEvents(true),
                (ServerLevel) died.level()
            ); // Prevents infinite death event recursion

            for (int i = 0; i < randomIndex - 1; i++) {
                if (slicedEntity.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
                    break;
                }
                slicedEntity.hurt(source, Float.MAX_VALUE);
                slicedEntity = (LivingEntity) slicedEntity.getComponent(STACKED_ENTITIES).sliceOne(died.level());
            }

            slicedEntity.getComponent(STACKED_ENTITIES).applyConsumerToAll(
                e -> e.getComponent(STACKED_ENTITIES).setSkipDeathEvents(false),
                (ServerLevel) died.level()
            ); // Re-enable events

            died.getComponent(STACKED_ENTITIES).sliceOne(died.level());

        } else if (config.deathAction == SLICE) {
            died.getComponent(STACKED_ENTITIES).sliceOne(died.level());

        } else if (config.deathAction == BY_DAMAGE) {
            float damage = died.getComponent(STACKED_ENTITIES).getLastHurtValue();
            damage -= died.getComponent(STACKED_ENTITIES).getLastHpValue();

            LivingEntity slicedEntity = (LivingEntity) died.getComponent(STACKED_ENTITIES).sliceOne(died.level());
            slicedEntity.getComponent(STACKED_ENTITIES).applyConsumerToAll(
                e -> e.getComponent(STACKED_ENTITIES).setSkipDeathEvents(true),
                (ServerLevel) died.level()
            ); // Prevents infinite death event recursion

            while (damage > 0f) {
                float hpLeft = slicedEntity.getHealth();
                slicedEntity.hurt(source, damage);
                damage -= hpLeft;
                if (slicedEntity.isDeadOrDying() && !slicedEntity.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
                    slicedEntity = (LivingEntity) slicedEntity.getComponent(STACKED_ENTITIES).sliceOne(slicedEntity.level());
                }

                if (slicedEntity.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
                    break;
                }
            }

            slicedEntity.getComponent(STACKED_ENTITIES).applyConsumerToAll(
                e -> e.getComponent(STACKED_ENTITIES).setSkipDeathEvents(false),
                (ServerLevel) died.level()
            ); // Re-enable events
        }
    }

    /**
     * Called when an entity is tamed. Handles the taming of the entity based on the config.
     * Called by
     * {@link com.voxelbuster.stackmobfabric.mixin.MixinTamableAnimal#injectTame(Player, CallbackInfo) MixinTamableAnimal}.
     *
     * @param entity      The entity that was tamed
     * @param triggeredBy The player that tamed the entity
     */
    public void onEntityTame(TamableAnimal entity, Player triggeredBy) {
        if (entity.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (entity.level().isClientSide()) {
            return;
        }

        if (config.tamingAction == EnumModifyHandlingAction.ALL) {
            entity.getComponent(STACKED_ENTITIES).applyConsumerToAll(
                e -> ((TamableAnimal) e).tame(triggeredBy), (ServerLevel) entity.level()
            );
        } else if (config.tamingAction == EnumModifyHandlingAction.SLICE) {
            entity.getComponent(STACKED_ENTITIES).sliceOne(entity.level());
        }
    }

    /**
     * Called when an entity starts love mode.
     * Handles the breeding of the entity based on the config.
     * Creates a scheduler task to spawn the babies if needed.
     * Called by
     * {@link com.voxelbuster.stackmobfabric.mixin.MixinAnimal#injectSetInLove(Player, CallbackInfo) MixinAnimal}
     *
     * @param parent      The entity that started love mode
     * @param triggeredBy The player that triggered the love mode
     */
    public void onEntityLoveMode(Animal parent, Player triggeredBy) {
        if (parent.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (parent.level().isClientSide()) {
            return;
        }

        if (config.breedAction == EnumModifyHandlingAction.ALL) {
            int count = triggeredBy.getMainHandItem().getCount();
            int stackSize = parent.getComponent(STACKED_NAMEABLE).getStackSize();
            int babies = Math.min(stackSize, count) / 2;

            scheduler.schedule(new Scheduler.Task(() -> {
                for (int i = 0; i < babies; i++) {
                    parent.spawnChildFromBreeding((ServerLevel) parent.level(), parent);
                }

                parent.getComponent(STACKED_ENTITIES).applyConsumerToAll(
                    e -> ((Animal) e).setAge(Animal.PARENT_AGE_AFTER_BREEDING), (ServerLevel) parent.level()
                );
            }, 40L));

            triggeredBy.getMainHandItem()
                .setCount(triggeredBy.getMainHandItem().getCount() - Math.min(count, stackSize) + 1);
        } else if (config.breedAction == EnumModifyHandlingAction.SLICE) {
            parent.getComponent(STACKED_ENTITIES).sliceOne(parent.level());
        }

    }

    /**
     * Called when an entity is renamed. Sets the custom name component of the entity.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinNameTagItem#injectInteractLivingEntity MixinNameTagItem}
     *
     * @param entity  The entity that was renamed
     * @param newName The text Component for the new name of the entity
     */
    public void onEntityRename(@NonNull Entity entity, @NonNull Component newName) {
        entity.getComponent(STACKED_NAMEABLE).setCustomName(newName.getString());

        if (config.renameAction == EnumModifyHandlingAction.SLICE) {
            entity.getComponent(STACKED_ENTITIES).sliceOne(entity.level());
        } else if (config.renameAction == EnumModifyHandlingAction.ALL) {
            entity.getComponent(STACKED_ENTITIES).applyConsumerToAllWithoutProvider(
                e -> e.getComponent(STACKED_NAMEABLE).setCustomName(newName.getString()), (ServerLevel) entity.level()
            );
        }
    }

    /**
     * Called when a chicken lays an egg. Handles item drops based on the config.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinChicken#injectAiStep(CallbackInfo) MixinChicken}
     *
     * @param parent The chicken that laid the egg
     */
    public void onChickenLayEgg(@NonNull Chicken parent) {
        if (!config.applyToLiveDrops) {
            return;
        }

        int stackSize = parent.getComponent(STACKED_NAMEABLE).getStackSize() - 1;
        int itemStackSize = Items.EGG.getDefaultMaxStackSize();

        while (stackSize > 0){
            parent.spawnAtLocation(new ItemStack(Items.EGG, itemStackSize));
            stackSize -= itemStackSize;
        }
    }

    /**
     * Called when a baby entity grows into an adult.
     * Automatically makes all stacked baby entities grow into adults.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinAgeableMob#injectAgeBoundaryReached(CallbackInfo) MixinAgeableMob}
     *
     * @param ageableMob The entity that grew
     */
    public void onEntityGrow(AgeableMob ageableMob) {
        if (ageableMob.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (ageableMob.level().isClientSide()) {
            return;
        }

        ageableMob.getComponent(STACKED_ENTITIES).applyConsumerToAll(
            e -> ((AgeableMob) e).ageUp(ageableMob.getAge()), (ServerLevel) ageableMob.level()
        );
    }

    /**
     * Called when a turtle drops a scute. Handles item drops based on the config.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinTurtle#injectAgeBoundaryReached(CallbackInfo) MixinTurtle}
     *
     * @param turtle The turtle that dropped the scute
     */
    public void onTurtleDropScute(Turtle turtle) {
        if (!config.applyToLiveDrops) {
            return;
        }

        int additionalScutes = turtle.getComponent(STACKED_NAMEABLE).getStackSize() - 1;
        turtle.spawnAtLocation(new ItemStack(Items.TURTLE_SCUTE, additionalScutes));
    }

    /**
     * Called when a sheep is sheared. Handles shearing based on the config.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinSheep#injectMobInteract(Player, InteractionHand, CallbackInfoReturnable) MixinSheep}
     *
     * @param sheared The sheep that was sheared
     * @param by      The player that sheared the sheep
     */
    @SuppressWarnings("unused")
    public void onSheepShear(Sheep sheared, Player by) {
        if (sheared.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (sheared.level().isClientSide()) {
            return;
        }

        if (config.shearAction == EnumModifyHandlingAction.ALL) {
            sheared.getComponent(STACKED_ENTITIES).applyConsumerToAll(
                e -> {
                    e.setPos(sheared.getEyePosition());
                    ((Sheep) e).shear(SoundSource.PLAYERS);
                }, (ServerLevel) sheared.level()
            );
        } else if (config.shearAction == EnumModifyHandlingAction.SLICE) {
            sheared.getComponent(STACKED_ENTITIES).sliceOne(sheared.level());
        }
    }

    /**
     * Called when a sheep changes color. Handles the change based on the config.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinSheep#injectSetColor(DyeColor, CallbackInfo) MixinSheep}
     *
     * @param dyed  The sheep that was dyed
     * @param color The color the sheep was dyed
     */
    public void onDyeSheep(Sheep dyed, DyeColor color) {
        if (dyed.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (dyed.level().isClientSide()) {
            return;
        }

        if (config.dyeAction == EnumModifyHandlingAction.ALL) {
            dyed.getComponent(STACKED_ENTITIES).applyConsumerToAllWithoutProvider(
                e -> ((Sheep) e).setColor(color), (ServerLevel) dyed.level()
            );
        } else if (config.dyeAction == EnumModifyHandlingAction.SLICE) {
            dyed.getComponent(STACKED_ENTITIES).sliceOne(dyed.level());
        }
    }

    /**
     * Called when a slime splits into smaller slimes.
     * Handles the split based on the config.
     * This is called for each slime that is created from the split.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinSlime#injectRemove(Entity.RemovalReason, CallbackInfo, Slime) MixinSlime}
     *
     * @param split   The slime that died and was split
     * @param created The slime that was created from the split
     * @param reason  The reason the parent slime was removed
     */
    public void onSlimeSplit(Slime split, Slime created, Entity.RemovalReason reason) {
        if (!config.applyToSplitSlimes) {
            return;
        }

        if (Objects.equals(split.getLastDamageSource(), split.damageSources().genericKill())) {
            return;
        }

        if (reason.equals(Entity.RemovalReason.DISCARDED)) {
            return;
        }

        if (split.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (split.level().isClientSide()) {
            return;
        }

        created.getComponent(STACKED_ENTITIES)
            .getStackedEntities()
            .addAll(split.getComponent(STACKED_ENTITIES).getStackedEntities());

        created.getComponent(STACKED_ENTITIES)
            .applyConsumerToAll(
                e -> ((Slime) e).setSize(created.getSize(), true),
                (ServerLevel) created.level()
            );
    }

    /**
     * Called when a pet's collar is dyed.
     * Handles the change based on the config.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinWolf#injectMobInteract MixinWolf} and
     * {@link com.voxelbuster.stackmobfabric.mixin.MixinCat#injectMobInteract MixinCat}.
     *
     * @param dyed  The pet that was dyed
     * @param color The color the pet was dyed
     */
    public void onDyePet(TamableAnimal dyed, DyeColor color) {
        if (dyed.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (dyed.level().isClientSide()) {
            return;
        }

        if (dyed instanceof Wolf wolf) {
            if (config.dyeAction == EnumModifyHandlingAction.ALL) {
                wolf.getComponent(STACKED_ENTITIES).applyConsumerToAllWithoutProvider(
                    e -> ((Wolf) e).setCollarColor(color),
                    (ServerLevel) wolf.level()
                );
            } else if (config.dyeAction == EnumModifyHandlingAction.SLICE) {
                wolf.getComponent(STACKED_ENTITIES).sliceOne(wolf.level());
            }
        } else if (dyed instanceof Cat cat) {
            if (config.dyeAction == EnumModifyHandlingAction.ALL) {
                cat.getComponent(STACKED_ENTITIES).applyConsumerToAllWithoutProvider(
                    e -> ((Cat) e).setCollarColor(color),
                    (ServerLevel) cat.level()
                );
            } else if (config.dyeAction == EnumModifyHandlingAction.SLICE) {
                cat.getComponent(STACKED_ENTITIES).sliceOne(cat.level());
            }
        }
    }

    /**
     * Called when a bee pollinates a flower.
     * Splits all currently stacked bees into individual entities, so that they can enter a hive
     * and properly drop off their nectar.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinBee#injectSetHasNectar MixinBee}.
     *
     * @param bee The bee that collected nectar
     */
    public void onBeePollinate(Bee bee) {
        if (bee.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (bee.level().isClientSide()) {
            return;
        }

        bee.getComponent(STACKED_ENTITIES).applyConsumerToAllWithoutProvider(
            e -> ((Bee) e).setHasNectar(true), (ServerLevel) bee.level()
        );

        bee.getComponent(STACKED_ENTITIES).sliceAll(bee.level());
    }

    /**
     * Called when a sheep grows back their wool.
     * Regrows the wool on all sheep in the stack.
     * Called by {@link com.voxelbuster.stackmobfabric.mixin.MixinSheep#injectSetSheared MixinSheep}.
     *
     * @param sheep The sheep that regrew their wool
     */
    public void onSheepGrowWool(Sheep sheep) {
        if (sheep.getComponent(STACKED_ENTITIES).getStackedEntities().isEmpty()) {
            return;
        }

        if (sheep.level().isClientSide()) {
            return;
        }

        sheep.getComponent(STACKED_ENTITIES).applyConsumerToAllWithoutProvider(
            e -> ((Sheep) e).setSheared(false), (ServerLevel) sheep.level()
        );
    }
}

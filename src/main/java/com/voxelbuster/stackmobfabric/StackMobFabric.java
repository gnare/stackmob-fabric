package com.voxelbuster.stackmobfabric;

import com.mojang.logging.LogUtils;
import com.voxelbuster.stackmobfabric.config.ModConfig;
import com.voxelbuster.stackmobfabric.event.ModEntityListener;
import lombok.Getter;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.Toml4jConfigSerializer;
import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;

@Getter
public class StackMobFabric implements ModInitializer {
    private static final Logger log = LogUtils.getLogger();

    @Getter
    private static StackMobFabric instance;

    public static ModConfig config;

    private final ModEntityListener entityListener = new ModEntityListener();

    public StackMobFabric() {
        instance = this;
    }

    @Override
    public void onInitialize() {
        log.info("Starting StackMobFabric...");

        entityListener.register();
        AutoConfig.register(ModConfig.class, Toml4jConfigSerializer::new);

        config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
    }
}

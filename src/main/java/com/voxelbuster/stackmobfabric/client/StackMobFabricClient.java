package com.voxelbuster.stackmobfabric.client;

import net.fabricmc.api.ClientModInitializer;

/**
 * Dummy class to initialize the client side of the mod. Not needed currently.
 */
public class StackMobFabricClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {

    }
}

package com.voxelbuster.stackmobfabric.util;

public class MathUtil {
    public static boolean near(int a, int b, int range) {
        return Math.abs(a - b) <= range;
    }
}

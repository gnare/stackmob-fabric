package com.voxelbuster.stackmobfabric.util;

public final class Constants {
    public static final String MOD_ID = "stackmobfabric";
    public static final String MOD_NAME = "StackMob Fabric";
    public static final String MOD_VERSION = "1.0.1";

    public static final String MC_VERSION = "1.20.4";
    public static final String LOADER_VERSION = "0.15.9";
}

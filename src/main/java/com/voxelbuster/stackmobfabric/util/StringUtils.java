package com.voxelbuster.stackmobfabric.util;

import java.util.Arrays;
import java.util.stream.Collectors;

public final class StringUtils {
    public static String capitalizeName(String name) {
        return Arrays.stream(name.split(" "))
            .map(word -> word.substring(0, 1).toUpperCase() + word.substring(1))
            .collect(Collectors.joining(" "));
    }
}

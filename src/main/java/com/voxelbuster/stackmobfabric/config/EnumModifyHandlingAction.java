package com.voxelbuster.stackmobfabric.config;

public enum EnumModifyHandlingAction {
    SLICE,
    ALL,
    NONE
}

package com.voxelbuster.stackmobfabric.config;

public enum EnumDeathHandlingAction {
    SLICE,
    RANDOM,
    BY_DAMAGE,
    ALL
}

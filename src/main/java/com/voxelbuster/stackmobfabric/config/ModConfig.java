package com.voxelbuster.stackmobfabric.config;

import com.voxelbuster.stackmobfabric.util.Constants;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

/**
 * Configuration class for the mod.
 * Used by AutoConfig to generate the config screen.
 */
@SuppressWarnings({"FieldMayBeFinal"})
@Config(name = Constants.MOD_ID)
public class ModConfig implements ConfigData {
    @ConfigEntry.Gui.Tooltip
    public boolean stackMobs = true;
    @ConfigEntry.Gui.Tooltip
    public boolean stackItems = true;
    @ConfigEntry.Gui.Tooltip
    public boolean requireLineOfSight = true;

    @ConfigEntry.Gui.Tooltip
    public boolean applyToSplitSlimes = true;
    @ConfigEntry.Gui.Tooltip
    public boolean increaseSlimeSize = true;
    @ConfigEntry.Gui.Tooltip
    public boolean applyToLiveDrops = true;

    @ConfigEntry.Gui.Tooltip
    public boolean stackVillagers = false;
    @ConfigEntry.Gui.Tooltip
    public boolean stackTamed = false;
    @ConfigEntry.Gui.Tooltip
    public boolean stackBees = true;

    @ConfigEntry.Gui.Tooltip
    public boolean stackBabies = true;

    @ConfigEntry.Gui.Tooltip
    public boolean stackNonBabies = true;

    @ConfigEntry.Gui.Tooltip(count = 3)
    @ConfigEntry.BoundedDiscrete(min = 1, max = 1000)
    public int processingRate = 10;
    @ConfigEntry.Gui.Tooltip(count = 3)
    @ConfigEntry.BoundedDiscrete(min = 1, max = 10000)
    public int processDelay = 200;
    @ConfigEntry.Gui.Tooltip(count = 3)
    @ConfigEntry.BoundedDiscrete(min = 1, max = 100)
    public int stackSearchRadius = 5;

    @ConfigEntry.Gui.Tooltip(count = 4)
    @ConfigEntry.Gui.EnumHandler(option = ConfigEntry.Gui.EnumHandler.EnumDisplayOption.BUTTON)
    public EnumDeathHandlingAction deathAction = EnumDeathHandlingAction.SLICE;

    @ConfigEntry.Gui.Tooltip(count = 4)
    @ConfigEntry.Gui.EnumHandler(option = ConfigEntry.Gui.EnumHandler.EnumDisplayOption.BUTTON)
    public EnumModifyHandlingAction dyeAction = EnumModifyHandlingAction.SLICE;

    @ConfigEntry.Gui.Tooltip(count = 4)
    @ConfigEntry.Gui.EnumHandler(option = ConfigEntry.Gui.EnumHandler.EnumDisplayOption.BUTTON)
    public EnumModifyHandlingAction renameAction = EnumModifyHandlingAction.SLICE;

    @ConfigEntry.Gui.Tooltip(count = 4)
    @ConfigEntry.Gui.EnumHandler(option = ConfigEntry.Gui.EnumHandler.EnumDisplayOption.BUTTON)
    public EnumModifyHandlingAction shearAction = EnumModifyHandlingAction.ALL;

    @ConfigEntry.Gui.Tooltip(count = 4)
    @ConfigEntry.Gui.EnumHandler(option = ConfigEntry.Gui.EnumHandler.EnumDisplayOption.BUTTON)
    public EnumModifyHandlingAction breedAction = EnumModifyHandlingAction.ALL;

    @ConfigEntry.Gui.Tooltip(count = 4)
    @ConfigEntry.Gui.EnumHandler(option = ConfigEntry.Gui.EnumHandler.EnumDisplayOption.BUTTON)
    public EnumModifyHandlingAction tamingAction = EnumModifyHandlingAction.SLICE;

    @ConfigEntry.Gui.Tooltip(count = 3)
    public List<String> entityTypeBlacklist = new ArrayList<>();

    @Override
    public void validatePostLoad() throws ValidationException {
        ConfigData.super.validatePostLoad();

        if (processingRate < 1) {
            processingRate = 1;
        }

        if (processDelay < 1) {
            processDelay = 1;
        }

        if (stackSearchRadius < 0) {
            stackSearchRadius = 0;
        }
    }

    protected ModConfig() {

    }

    @SuppressWarnings("squid:S1452") // Wildcard is needed
    public List<EntityType<? extends Entity>> getExcludedEntityTypes() {
        List<EntityType<? extends Entity>> excludedTypes = new ArrayList<>();

        for (String entityType : entityTypeBlacklist) {
            EntityType.byString(entityType).ifPresent(excludedTypes::add);
        }

        return excludedTypes;
    }
}

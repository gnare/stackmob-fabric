package com.voxelbuster.stackmobfabric.data;

import com.google.common.collect.Streams;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.voxelbuster.stackmobfabric.data.StackMobComponents.STACKED_ENTITIES;
import static com.voxelbuster.stackmobfabric.data.StackMobComponents.STACKED_NAMEABLE;

@Getter
public class StackedEntityComponentImpl implements IStackedEntityComponent {
    private final ArrayList<CompoundTag> stackedEntities = new ArrayList<>();

    @Setter
    private float lastHurtValue = 0f;

    @Setter
    private float lastHpValue;

    @Setter
    @Getter(AccessLevel.PRIVATE)
    private boolean skipDeathEvents = false;

    private final Entity provider;

    public StackedEntityComponentImpl(Entity provider) {
        this.provider = provider;
        lastHpValue = (provider instanceof LivingEntity le) ? le.getHealth() : 0f;
    }

    public void applyConsumerToAllWithoutProvider(Consumer<Entity> function, ServerLevel world) {
        HashMap<CompoundTag, CompoundTag> overwriteMap = new HashMap<>();

        stackedEntities.forEach(stackTag -> {
            Optional<Entity> entityWrapper = EntityType.create(stackTag, world);
            entityWrapper.ifPresent(entity -> {
                function.accept(entity);
                CompoundTag newTag = new CompoundTag();
                entity.save(newTag);
                overwriteMap.put(stackTag, newTag);
                entity.remove(Entity.RemovalReason.DISCARDED);
            });
        });

        overwriteMap.forEach((oldTag, newTag) -> {
            int insertIndex = stackedEntities.indexOf(oldTag);
            stackedEntities.set(insertIndex, newTag);
        });
    }

    @Override
    public void applyConsumerToAll(Consumer<Entity> function, ServerLevel world) {
        applyConsumerToAllWithoutProvider(function, world);

        function.accept(provider);
    }

    @Override
    public <T> Collection<T> applyFunctionToAll(Function<Entity, T> function, Level world) {
        HashMap<CompoundTag, CompoundTag> overwriteMap = new HashMap<>();

        ArrayList<T> results = new ArrayList<>();

        stackedEntities.forEach(stackTag -> {
            Optional<Entity> entityWrapper = EntityType.create(stackTag, world);
            entityWrapper.ifPresent(entity -> {
                results.add(function.apply(entity));
                CompoundTag newTag = new CompoundTag();
                entity.save(newTag);
                overwriteMap.put(stackTag, newTag);
                entity.remove(Entity.RemovalReason.DISCARDED);
            });
        });

        overwriteMap.forEach((oldTag, newTag) -> {
            int insertIndex = stackedEntities.indexOf(oldTag);
            stackedEntities.remove(insertIndex);
            stackedEntities.set(insertIndex, newTag);
        });

        results.add(function.apply(provider));

        return results;
    }

    @Override
    public Entity sliceOne(Level world) {
        if (stackedEntities.isEmpty()) {
            return provider;
        }

        CompoundTag tag = stackedEntities.remove(0);
        Optional<Entity> optionalEntity = EntityType.create(tag, world);
        if (optionalEntity.isPresent()) {
            // Create the entity
            Entity entity = optionalEntity.get();
            entity.setPos(provider.getX(), provider.getY(), provider.getZ());
            entity.tickCount = 0;
            entity.invulnerableTime = 5;
            entity.getComponent(STACKED_NAMEABLE).setStackSize(stackedEntities.size() + 1);

            // Move the stored entities to the new entity
            entity.getComponent(STACKED_ENTITIES)
                .getStackedEntities()
                .addAll(provider.getComponent(STACKED_ENTITIES).getStackedEntities());

            // Clear the provider's stored entities
            provider.getComponent(STACKED_ENTITIES).getStackedEntities().clear();

            // Spawn the new entity
            world.addFreshEntity(entity);

            // Set the new entity's stack size

            return entity;
        }

        return null;
    }

    @Override
    public List<Entity> sliceAll(Level level) {
        List<Entity> result = List.copyOf(stackedEntities)
            .stream()
            .flatMap(
                tag -> {
                    Entity e = sliceOne(level);
                    Stream<Entity> s = e.getComponent(STACKED_ENTITIES).sliceAll(level).stream();
                    return Streams.concat(Stream.of(e), s);
                }
            )
            .toList();
        stackedEntities.clear();
        result.forEach(entity -> entity.getComponent(STACKED_NAMEABLE).setStackSize(1));
        return result;
    }

    @Override
    public void readFromNbt(CompoundTag tag, HolderLookup.Provider registryLookup) {
        stackedEntities.clear();
        tag.getList("StackedEntities", 10).forEach(stackTag -> {
            if (stackTag instanceof CompoundTag compoundTag) {
                stackedEntities.add(compoundTag);
            }
        });

        lastHurtValue = tag.getFloat("LastHurtValue");
    }

    @Override
    public void writeToNbt(CompoundTag tag, HolderLookup.Provider registryLookup) {
        ListTag listTag = new ListTag();
        listTag.addAll(stackedEntities);
        tag.put("StackedEntities", listTag);
        tag.putFloat("LastHurtValue", lastHurtValue);
    }

    @Override
    public Entity getProvider() {
        return provider;
    }

    @Override
    public boolean shouldSkipDeathEvents() {
        return isSkipDeathEvents();
    }
}

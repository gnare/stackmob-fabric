package com.voxelbuster.stackmobfabric.data;

import org.ladysnake.cca.api.v3.component.ComponentV3;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * This component is used to store the entities that are stacked on top of the parent entity.
 * Stores all entities as CompoundTags in a list. This allows them to be automatically
 * encoded or decoded by Minecraft as needed without losing any data.
 */
public interface IStackedEntityComponent extends ComponentV3 {
    /**
     * Gets the list of stacked entities attached to this entity.
     *
     * @return List of CompoundTags representing the stacked entities.
     */
    List<CompoundTag> getStackedEntities();

    /**
     * Gets the last damage value dealt to the parent entity. Needed for
     * certain mod events.
     */
    float getLastHurtValue();

    /**
     * Gets the last health value of the parent entity before being damaged. Needed for certain mod events.
     */
    float getLastHpValue();

    /**
     * Gets whether the parent entity should skip processing modded death events.
     */
    boolean shouldSkipDeathEvents();

    /**
     * Sets whether the parent entity should skip processing modded death events.
     */
    void setSkipDeathEvents(boolean value);

    /**
     * Sets the last health value of the parent entity before being damaged.
     */
    void setLastHpValue(float value);

    /**
     * Sets the last damage value dealt to the parent entity.
     */
    void setLastHurtValue(float value);

    /**
     * Applies a consumer function to all stacked entities, as well as the parent entity.
     * This will mutate the internal list of stacked entities. Make sure not to call any function
     * that will cause infinite recursion.
     *
     * @param function The function to apply to each entity
     * @param world    The world the entities are in
     */
    void applyConsumerToAll(Consumer<Entity> function, ServerLevel world);

    /**
     * Applies a consumer function to all stacked entities.
     * This will mutate the internal list of stacked entities.
     * Does not apply to the parent entity (to avoid infinite recursion where necessary).
     *
     * @param function The function to apply to each entity
     * @param world    The world the entities are in
     */
    void applyConsumerToAllWithoutProvider(Consumer<Entity> function, ServerLevel world);

    /**
     * Applies a function to all stacked entities, as well as the parent entity.
     * This will mutate the internal list of stacked entities. Make sure not to call any function
     * that will cause infinite recursion.
     *
     * @param function The function to apply to each entity
     * @param world    The world the entities are in
     * @return A collection of the results of the function applied to each entity
     */
    @SuppressWarnings("unused")
    <T> Collection<T> applyFunctionToAll(Function<Entity, T> function, Level world);

    /**
     * This should return the new parent entity after the split, not the current parent entity.
     * Additionally, the old parent entity should have the stored entities moved to the new parent entity.
     *
     * @param world The world the entities are in
     * @return The new parent entity
     */
    Entity sliceOne(Level world);

    /**
     * Breaks the entire stack of entities into individual entities.
     * This will respawn each entity into the world at the location of the parent entity.
     * Additionally, clears the list of stacked entities.
     *
     * @param level The world the entities are in
     * @return A list of all entities that were respawned
     */
    List<Entity> sliceAll(Level level);

    /**
     * Gets the parent entity that the stacked entities are attached to.
     */
    @SuppressWarnings("unused")
    Entity getProvider();

}

package com.voxelbuster.stackmobfabric.data;

import org.ladysnake.cca.api.v3.component.ComponentKey;
import org.ladysnake.cca.api.v3.component.ComponentRegistryV3;
import org.ladysnake.cca.api.v3.entity.EntityComponentFactoryRegistry;
import org.ladysnake.cca.api.v3.entity.EntityComponentInitializer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;

import static com.voxelbuster.stackmobfabric.util.Constants.MOD_ID;

public class StackMobComponents implements EntityComponentInitializer {
    public static final ComponentKey<IStackedEntityComponent> STACKED_ENTITIES =
        ComponentRegistryV3.INSTANCE.getOrCreate(
            ResourceLocation.tryBuild(MOD_ID, "stacked_entities"),
            IStackedEntityComponent.class
        );

    public static final ComponentKey<IStackedNameableComponent> STACKED_NAMEABLE =
        ComponentRegistryV3.INSTANCE.getOrCreate(
            ResourceLocation.tryBuild(MOD_ID, "stacked_nameable"),
            IStackedNameableComponent.class
        );

    @Override
    public void registerEntityComponentFactories(EntityComponentFactoryRegistry registry) {
        registry.registerFor(
            LivingEntity.class,
            STACKED_ENTITIES,
            StackedEntityComponentImpl::new
        );

        registry.registerFor(
            ItemEntity.class,
            STACKED_ENTITIES,
            StackedEntityComponentImpl::new
        );

        registry.registerFor(
            LivingEntity.class,
            STACKED_NAMEABLE,
            StackedNameableComponentImpl::new
        );

        registry.registerFor(
            ItemEntity.class,
            STACKED_NAMEABLE,
            StackedNameableComponentImpl::new
        );
    }
}

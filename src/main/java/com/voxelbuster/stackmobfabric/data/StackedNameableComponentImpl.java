package com.voxelbuster.stackmobfabric.data;

import lombok.Getter;
import lombok.NonNull;
import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.contents.PlainTextContents;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

import static com.voxelbuster.stackmobfabric.util.StringUtils.capitalizeName;

@Getter
public class StackedNameableComponentImpl implements IStackedNameableComponent {

    private final Entity provider;

    @NonNull
    public String customName = "";

    private int stackSize = 1;

    public StackedNameableComponentImpl(@NonNull Entity provider) {
        this.provider = provider;
    }

    @Override
    public String getDefaultName() {
        if (provider instanceof ItemEntity item) {
            return capitalizeName(item.getItem().getDisplayName().getString());
        }

        return capitalizeName(provider.getType().toShortString());
    }

    @Override
    public String getName() {
        if (StringUtils.isBlank(customName)) {
            return stackSize > 1 ? getDefaultName() + " x" + stackSize : getDefaultName();
        } else {
            return stackSize > 1 ? customName + " x" + stackSize : customName;
        }
    }

    public void setCustomName(String customName) {
        this.customName = Objects.requireNonNullElse(customName, "");
        provider.setCustomName(
            MutableComponent.create(new PlainTextContents.LiteralContents(
                getName()
            ))
        );
    }

    public void setStackSize(int stackSize) {
        this.stackSize = Math.max(stackSize, 1);
        setCustomName(customName);
    }

    @Override
    public void readFromNbt(CompoundTag tag, HolderLookup.Provider registryLookup) {
        tag.putString("StackMobCustomName", customName);
        tag.putInt("StackMobStackSize", stackSize);
    }

    @Override
    public void writeToNbt(CompoundTag tag, HolderLookup.Provider registryLookup) {
        customName = Objects.requireNonNullElse(tag.getString("StackMobCustomName"), "");
        stackSize = tag.getInt("StackMobStackSize");
    }
}

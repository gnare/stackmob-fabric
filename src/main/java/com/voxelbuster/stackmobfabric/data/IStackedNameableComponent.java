package com.voxelbuster.stackmobfabric.data;

import org.ladysnake.cca.api.v3.component.ComponentV3;

/**
 * A component that provides a name, custom name, default name, and stack size for an entity.
 * Necessary to properly show the number of entities stacked, while still allowing players to rename them with name
 * tags.
 */
public interface IStackedNameableComponent extends ComponentV3 {
    /**
     * Gets the full display name of the entity, including the stack size.
     */
    String getName();

    /**
     * Gets the custom name set by a name tag, if available.
     */
    String getCustomName();

    /**
     * Gets the default name of the entity, based on the current language.
     */
    String getDefaultName();

    /**
     * Gets the size of the stack on the current entity.
     */
    int getStackSize();

    /**
     * Sets the size of the stack on the current entity.
     */
    void setStackSize(int stackSize);

    /**
     * Sets the custom name of the entity. Used for name tags.
     */
    void setCustomName(String customName);
}

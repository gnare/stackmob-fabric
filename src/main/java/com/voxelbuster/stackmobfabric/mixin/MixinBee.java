package com.voxelbuster.stackmobfabric.mixin;

import com.voxelbuster.stackmobfabric.StackMobFabric;
import net.minecraft.world.entity.animal.Bee;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Bee.class)
public class MixinBee {
    @Inject(
        method = "setHasNectar",
        at = @At("TAIL")
    )
    public void injectSetHasNectar(boolean hasNectar, CallbackInfo ci) {
        if (hasNectar) {
            StackMobFabric.getInstance().getEntityListener().onBeePollinate((Bee) (Object) this);
        }
    }
}

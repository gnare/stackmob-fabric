package com.voxelbuster.stackmobfabric.mixin;

import com.llamalad7.mixinextras.sugar.Local;
import com.voxelbuster.stackmobfabric.StackMobFabric;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.animal.Cat;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Cat.class)
public class MixinCat {

    @Inject(
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/world/entity/animal/Cat;setCollarColor(Lnet/minecraft/world/item/DyeColor;)V"
        ),
        method = "mobInteract(Lnet/minecraft/world/entity/player/Player;Lnet/minecraft/world/InteractionHand;)Lnet/minecraft/world/InteractionResult;"
    )
    public void injectMobInteract(
        Player player,
        InteractionHand hand,
        CallbackInfoReturnable<InteractionResult> cir,
        @Local DyeColor color
    ) {
        StackMobFabric.getInstance().getEntityListener().onDyePet((Cat) (Object) this, color);
    }
}

package com.voxelbuster.stackmobfabric.mixin;

import com.voxelbuster.stackmobfabric.StackMobFabric;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.animal.Sheep;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Sheep.class)
public class MixinSheep {
    @Inject(
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/world/entity/animal/Sheep;shear(Lnet/minecraft/sounds/SoundSource;)V",
            shift = At.Shift.AFTER,
            by = 1
        ),
        method = "mobInteract(Lnet/minecraft/world/entity/player/Player;Lnet/minecraft/world/InteractionHand;)Lnet/minecraft/world/InteractionResult;"
    )
    public void injectMobInteract(Player player, InteractionHand hand, CallbackInfoReturnable<InteractionResult> cir) {
        StackMobFabric.getInstance().getEntityListener().onSheepShear((Sheep) (Object) this, player);
    }

    @Inject(
        at = @At("TAIL"),
        method = "setSheared(Z)V"
    )
    public void injectSetSheared(boolean sheared, CallbackInfo ci) {
        if (!sheared) {
            StackMobFabric.getInstance().getEntityListener().onSheepGrowWool((Sheep) (Object) this);
        }
    }

    @Inject(
        at = @At("TAIL"),
        method = "setColor(Lnet/minecraft/world/item/DyeColor;)V"
    )
    public void injectSetColor(DyeColor color, CallbackInfo ci) {
        StackMobFabric.getInstance().getEntityListener().onDyeSheep((Sheep) (Object) this, color);
    }
}

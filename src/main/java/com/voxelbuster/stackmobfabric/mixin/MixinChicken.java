package com.voxelbuster.stackmobfabric.mixin;

import com.voxelbuster.stackmobfabric.StackMobFabric;
import net.minecraft.world.entity.animal.Chicken;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Chicken.class)
public class MixinChicken {
    @Inject(
        at = @At(value = "INVOKE_ASSIGN", target = "Lnet/minecraft/world/entity/animal/Chicken;spawnAtLocation(Lnet/minecraft/world/level/ItemLike;)Lnet/minecraft/world/entity/item/ItemEntity;"),
        method = "aiStep()V"
    )
    public void injectAiStep(CallbackInfo ci) {
        StackMobFabric.getInstance().getEntityListener().onChickenLayEgg((Chicken) (Object) this);
    }
}

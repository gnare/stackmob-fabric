package com.voxelbuster.stackmobfabric.mixin;

import com.voxelbuster.stackmobfabric.StackMobFabric;
import net.minecraft.world.entity.animal.Turtle;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Turtle.class)
public class MixinTurtle {

    @Inject(
        at = @At(value = "INVOKE_ASSIGN", target = "Lnet/minecraft/world/entity/animal/Turtle;spawnAtLocation(Lnet/minecraft/world/level/ItemLike;I)Lnet/minecraft/world/entity/item/ItemEntity;"),
        method = "ageBoundaryReached()V"
    )
    public void injectAgeBoundaryReached(CallbackInfo ci) {
        StackMobFabric.getInstance().getEntityListener().onTurtleDropScute((Turtle) (Object) this);
    }
}

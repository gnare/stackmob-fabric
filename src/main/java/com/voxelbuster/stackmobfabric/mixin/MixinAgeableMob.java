package com.voxelbuster.stackmobfabric.mixin;

import com.voxelbuster.stackmobfabric.StackMobFabric;
import net.minecraft.world.entity.AgeableMob;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(AgeableMob.class)
public class MixinAgeableMob {

    @Inject(
        at = @At("TAIL"),
        method = "ageBoundaryReached()V"
    )
    public void injectAgeBoundaryReached(CallbackInfo ci) {
        StackMobFabric.getInstance().getEntityListener().onEntityGrow((AgeableMob) (Object) this);
    }
}

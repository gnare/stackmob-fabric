# StackMob Fabric

---

A mod that allows players to stack massive amounts of mobs into a single entity.

Reduces lag without breaking mob farms.

Inspired by Nathat23's [StackMob5](https://github.com/Nathat23/StackMob-5) for Spigot.

### Mod Requirements
- [Fabric API](https://modrinth.com/mod/fabric-api)
- [Cardinal Components API](https://modrinth.com/mod/cardinal-components-api)
- [Cloth Config API](https://modrinth.com/mod/cloth-config)

#### Optional:
- [Mod Menu](https://modrinth.com/mod/modmenu)

---

### License

[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

---

### Contributing

Fork the repository on the platform of your choice, and make a merge request when finished.
Please make your MR for the `dev` branch. Keep your code clean and optimize your imports.
I reserve the right to reject MRs on the basis of poor code quality.

---

### Issues

Please submit all issues and bug reports to the [issue tracker](https://gitlab.com/gnare/stackmob-fabric/-/issues) 
Include your logs and modlist.

### Mod Conflicts
These mods have been confirmed to conflict with this mod. Make sure you have removed these conflicting mods _before_ submitting an issue report.

 - **Clumps** : provides similar functionality and creates a data race with StackMob's event handlers
